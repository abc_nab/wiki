# Background
ABC Bank wants to integrate with Third Parties like (Viettel, Vinanphone, Mobifone) for selling Prepiad SIM Card on theirs website

# Assumption
- This is an existing system, infrastructure already there, we just build new microservice to serve for new requirement
- Security
    - Should have the UAA system, all microservices will use this system for authorization and authentication for all apis/resources
    - For my implementation, I will ignore UAA system, just use basic authentication only
    - All sensitive information in configuration must `cipher` it.

# Design Considerations/ Decisions
- Create new microservice called `Mobile Privilege` for integrate with third parties
- Technologies
    - Java 11
    - Spring Boot: 2.4.4
    - Spring Cloud: 2020.0.2
    - Kafka for handle messaging
    - Database: MySQL. H2 for local testing

# High Level Design
## Short term solution - call to third party directly each time customer buy a Voucher
![img.png](img.png)

### Limitation
- Response late from third party (update 120 seconds) depends on network traffic at that time
- In case that the code can't be returned in-time, need to send code to customer via SMS
- Due to the response time from Third Parties, it will affect to user's experience when they use service from ABC Bank

## Long term solution
- Introduce `Voucher` service to store voucher codes from Third Parties
- ABC Bank need to talk with Third Party to reserve codes first and import codes to ABC system.
- All transactions just happen inside ABC system only
- Reduce late response time

![img_1.png](img_1.png)

# Low Level Design and implementation
- [ABC BFF - README](https://gitlab.com/abc_nab/abc-bff)
- [Mobile Privilege - README](https://gitlab.com/abc_nab/mobile-privilege)

# How to run service on you local
- Install and start kafka
    - Download and start manually. Please refer to Step#1 Step#2 from [kafka page](https://kafka.apache.org/quickstart)
    - More detail, please see [README of ABC-BFF](https://gitlab.com/abc_nab/abc-bff)
- Eureka server - Port 8761
    - Clone source code from https://gitlab.com/abc_nab/eureka
    - Run: `./mvnw`
- Config server - Port 8888
    - Clone source code from https://gitlab.com/abc_nab/config-server
    - Cloud config repository: https://github.com/minhchau110883/abc_nab-cloud-config  
    - Run: `./mvnw`
- Gateway service - Port 8080
    - Clone source code from https://gitlab.com/abc_nab/gateway
    - Run: `./mvnw`
- Mobile Privilege service - Port 8081
    - Clone source code from https://gitlab.com/abc_nab/mobile-privilege
    - Run: `./mvnw`
- ABC BFF service - Port 8082
    - Clone source code from https://gitlab.com/abc_nab/abc-bff
    - Run: `./mvnw`

# How to verify service on your local
Please follow [README of ABC-BFF](https://gitlab.com/abc_nab/abc-bff)

# Code folder structure - Apply the same for all services
- src/main/java
    - <base_package>
        - config -> handle configuration
        - constants -> handle constants
        - domain -> domain object, mapping with table/collection in db
        - messaging -> handle processing messaging, like kafka
        - repository -> handle CRUD
        - service -> business processing
            - dto -> data transfer object, use at rest controller
            - exception -> handle exception at service layer, include internal/external service
            - external -> communication with external service, like third parties
            - internal -> communication with another internal service
            - impl -> detail implementation of service layer
            - mapper -> define mapping between `dto` and `domain`
        - web
            - exception -> handle exception at controller layer
            - restcontroller -> Detail rest controller
            - util -> util
    - resources
        - db.changelogs -> handle db change log
        - http-client -> provide example http request for calling api
- src/main/test - Testing

# Remaining tasks
Because the limit of time, so there are some remaining tasks like
- Handle distributed tracking
- Create Docker for easy to run on local


